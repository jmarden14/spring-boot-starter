package groovy.net.spring.boot.starter.controllers

import net.spring.boot.starter.configuration.WebConfiguration
import net.spring.boot.starter.controllers.HelloWorldController
import net.spring.boot.starter.services.HelloWorldService
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@ContextConfiguration
@SpringBootTest(classes = [WebConfiguration], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class HelloWorldControllerTest extends Specification {

    private HelloWorldService helloWorldService = Mock()

    private HelloWorldController underTest = new HelloWorldController(helloWorldService: helloWorldService)

    private MockMvc mockMvc;

    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(underTest).build()
    }

    def "prints hello world with json content type"() {
        when:
            helloWorldService.printHello() >> "Hello World"
            def result = mockMvc.perform(get("/"))
        then:
            result.andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                    .andExpect(content().string("Hello World"))
    }
}
