package net.spring.boot.starter.services

import net.spring.boot.starter.configuration.WebConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

import javax.inject.Inject

@ContextConfiguration
@SpringBootTest(classes = [WebConfiguration], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class HelloWorldServiceTest extends Specification {

    @Inject
    private HelloWorldService underTest;

    def "prints hello world"() {

        when:
            def result = underTest.printHello()
        then:
            result == "Hello World"

    }
}
