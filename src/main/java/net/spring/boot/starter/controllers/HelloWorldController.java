package net.spring.boot.starter.controllers;

import net.spring.boot.starter.services.HelloWorldService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
public class HelloWorldController {

    @Inject
    private HelloWorldService helloWorldService;

    @RequestMapping(value = "/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String printHello() {
        return helloWorldService.printHello();
    }
}
