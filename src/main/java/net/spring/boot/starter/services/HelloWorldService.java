package net.spring.boot.starter.services;

import org.springframework.stereotype.Service;

@Service
public class HelloWorldService {

    public String printHello() {
        return "Hello World";
    }
}
